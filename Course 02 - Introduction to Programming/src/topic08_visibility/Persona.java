package topic08_visibility;

/*
  Curso: Introducción a la Programación.
  Tema 8: Privacidad, Abstracción y Encapsulación

  Ejercicio: Práctica de la Encapsulación.

  - Crear clase Persona.
  - Crear variables las privadas edad, nombre y telefono de la clase Persona.
  - Crear gets y sets de cada propiedad.
  - Crear un objeto persona en el main.
  - Utiliza los gets y sets para darle valores a las propiedades edad, nombre y 
    telefono, por último muéstralas por consola.
*/

/**
 *
 * @author José H. Albano
 */
public class Persona {
  private int edad;
  private String nombre;
  private String telefono;

  public int getEdad() {
    return edad;
  }

  public void setEdad(int edad) {
    this.edad = edad;
  }

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public String getTelefono() {
    return telefono;
  }

  public void setTelefono(String telefono) {
    this.telefono = telefono;
  }
}