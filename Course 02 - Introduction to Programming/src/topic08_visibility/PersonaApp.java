package topic08_visibility;

/*
  Curso: Introducción a la Programación.
  Tema 8: Privacidad, Abstracción y Encapsulación

  Ejercicio: Práctica de la Encapsulación.

  - Crear clase Persona.
  - Crear variables las privadas edad, nombre y telefono de la clase Persona.
  - Crear gets y sets de cada propiedad.
  - Crear un objeto persona en el main.
  - Utiliza los gets y sets para darle valores a las propiedades edad, nombre y 
    telefono, por último muéstralas por consola.
*/

/**
 *
 * @author José H. Albano
 */
public class PersonaApp {
  public static void main(String[] args) {
    System.out.println("*** Curso: Introducción a la Programación ***\n\n" +
      "--- Tema 8: Privacidad, Abstracción y Encapsulación ---\n\n" +
      "Ejercicio: Práctica de la Encapsulación\n" );

    Persona persona = new Persona();
    
    persona.setEdad(38);
    persona.setNombre("Cosme Fulanito");
    persona.setTelefono("+54 9 261 555444");
    
    System.out.println("\tLa Persona creada es:\n");
    System.out.println("\t" + persona.getNombre());
    System.out.println("\tEdad: " + persona.getEdad());
    System.out.println("\tTeléfono: " + persona.getTelefono());
  }
}