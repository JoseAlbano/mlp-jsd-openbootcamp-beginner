package topic09_inheritance;

/*
  Curso: Introducción a la Programación.
  Tema 9: Herencia, Polimorfismo e Interfaces

  Ejercicio: Práctica de la Herencia.

  - Crea una clase Persona con las siguientes variables:
    - La edad
    - El nombre
    - El teléfono
*/

/**
 *
 * @author José H. Albano
 */
public class Persona {
  private int edad;
  private String nombre;
  private String telefono;

  public int getEdad() {
    return edad;
  }

  public void setEdad(int edad) {
    this.edad = edad;
  }

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public String getTelefono() {
    return telefono;
  }

  public void setTelefono(String telefono) {
    this.telefono = telefono;
  }
}