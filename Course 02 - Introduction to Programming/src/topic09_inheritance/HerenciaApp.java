package topic09_inheritance;

/*
  Curso: Introducción a la Programación.
  Tema 9: Herencia, Polimorfismo e Interfaces

  Ejercicio: Práctica de la Herencia.

  - Crea ahora un objeto de la clase Cliente que debe tener como propiedades la
    edad, el telefono, el nombre y el credito, tienes que darles valor y 
    mostrarlas por pantalla.

    Adicional: Yo también agregué la creación de un objeto Trabajador.
*/

/**
 *
 * @author José H. Albano
 */
public class HerenciaApp {
  public static void main(String[] args) {
    System.out.println("*** Curso: Introducción a la Programación ***\n\n" +
      "--- Tema 9: Herencia, Polimorfismo e Interfaces ---\n\n" +
      "Ejercicio: Práctica de la Herenia\n" );

    Cliente cliente = new Cliente();
    Trabajador trabajador = new Trabajador();
        
    cliente.setNombre("Cosme Fulanito");
    cliente.setEdad(38);    
    cliente.setTelefono("+54 9 261 555444");
    cliente.setCredito(10000);
    
    trabajador.setNombre("Ace Ventura");
    trabajador.setEdad(30);
    trabajador.setTelefono("+54 9 261 333111");
    trabajador.setSalario(150000);
    
    
    System.out.println( "\tEl Cliente creado es: " );
    System.out.println( "\t" + cliente.getNombre() );
    System.out.println( "\tEdad: " + cliente.getEdad() );
    System.out.println( "\tTeléfono: " + cliente.getTelefono() );
    System.out.println( "\tCrédito: $" + cliente.getCredito() );
    
    System.out.println();
    
    System.out.println( "\tEl Trabajador creado es: " );
    System.out.println( "\t" + trabajador.getNombre() );
    System.out.println( "\tEdad: " + trabajador.getEdad() );
    System.out.println( "\tTeléfono: " + trabajador.getTelefono() );
    System.out.println( "\tSalario: $" + trabajador.getSalario() );
  }
}