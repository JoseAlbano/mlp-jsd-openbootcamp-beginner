package topic09_inheritance;

/*
  Curso: Introducción a la Programación.
  Tema 9: Herencia, Polimorfismo e Interfaces

  Ejercicio: Práctica de la Herencia.

  - Una vez creada la clase Persona, crea una nueva clase Cliente que herede de
    Persona, esta nueva clase tendrá la variable credito solo para esa clase.
*/

/**
 *
 * @author José H. Albano
 */
public class Cliente extends Persona {
  private double credito;

  public double getCredito() {
    return credito;
  }

  public void setCredito(double credito) {
    this.credito = credito;
  }
}