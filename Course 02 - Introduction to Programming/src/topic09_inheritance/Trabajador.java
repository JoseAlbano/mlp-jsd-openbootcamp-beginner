package topic09_inheritance;

/*
  Curso: Introducción a la Programación.
  Tema 9: Herencia, Polimorfismo e Interfaces

  Ejercicio: Práctica de la Herencia.

  - Una creada la clase Persona, haz que la clase Trabajador herede de Persona,
    y con una variable salario que solo tenga la clase Trabajador.
*/

/**
 *
 * @author José H. Albano
 */
public class Trabajador extends Persona {
  private double salario;

  public double getSalario() {
    return salario;
  }
  
  public void setSalario(double salario) {
    this.salario = salario;
  }
}