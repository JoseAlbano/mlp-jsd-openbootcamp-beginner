package topic03_functions;

/*
  Curso: Introducción a la Programación.
  Tema 3: Funciones

  Ejercicio - Segunda parte: Coche App
  
  - Crear una clase Coche.
  - Dentro de la clase Coche, una variable numérica que almacene el número de 
    puertas que tiene.
  - Una función que incremente el número de puertas que tiene el coche.
  - Crear un objeto miCoche en el main y añadirle una puerta.
  - Mostrar el número de puertas que tiene el objeto.
*/

/**
 * @author José Albano
 */
public class CocheApp {
  public static void main (String[] args) {
     System.out.println("*** Curso: Introducción a la Programación ***\n\n" +
      "--- Tema 3: Funciones ---\n\n" +
      "Ejercicio, 2da Parte: Coche App\n" );

    Coche miCoche = new Coche();

    miCoche.agregarPuerta();

    System.out.println( "\tEl Coche tiene " + miCoche.getNumeroPuertas() 
      + " Puerta(s).");
  }
}