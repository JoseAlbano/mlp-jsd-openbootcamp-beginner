package topic03_functions;

/* 
  Curso: Introducción a la Programación.
  Tema 3: Funciones

  Ejercicio - Segunda parte: Coche App
  
  - Crear una clase Coche.
  - Dentro de la clase Coche, una variable numérica que almacene el número de 
    puertas que tiene.
  - Una función que incremente el número de puertas que tiene el coche.
  - Crear un objeto miCoche en el main y añadirle una puerta.
  - Mostrar el número de puertas que tiene el objeto.
*/

/**
 * @author José Albano
 */
public class Coche {
  private int numeroPuertas;

  public void agregarPuerta() {
    numeroPuertas++;
  }

  public int getNumeroPuertas() {
    return numeroPuertas;
  }
}