package topic03_functions;

/*
  Curso: Introducción a la Programación.
  Tema 3: Funciones

  Ejercicio - Primera Parte: Suma
  
  - Crear una función con tres parámetros que sean números que se suman entre sí.
  - Llamar a la función en el main y darle valores.
*/

/**
 * @author José Albano
 */
public class SumaApp {

  public static void main(String[] args) {
    System.out.println("*** Curso: Introducción a la Programación ***\n\n" +
      "--- Tema 3: Funciones ---\n\n" +
      "Ejercicio, 1ra Parte: Suma\n" );

    sumarTresNumeros(10, 15, 25);
  }

  public static double sumarTresNumeros( double a, double b, double c) {
    return a + b + c;
  }
}