package topic04_control_statements;

/*
  Curso: Introducción a la Programación.
  Tema 4: Sentencias de Control

  Ejercicio: Práctica de Estructuras de Control.
*/

public class Tema4Ejercicios {
  public static void main(String[] args) {

    System.out.println("*** Curso: Introducción a la Programación ***\n\n" +
      "--- Tema 4: Sentencias de Control-- \n\n" +
      "Ejercicio: Práctica de Estructuras de Control" );

    /*
      Usando un if, crear una condición que compare si la variable numeroIf es
      positivo, negativo, o 0.

      Pista: Los números inferiores a 0 son negativos y los superiores, 
      positivos.
    */
    System.out.println("\n\t1) If:\n" );
    int numeroIf = 7;

    System.out.println("\t\tNúmero a comparar: " + numeroIf + "\n");

    if (numeroIf > 0 ) {
      System.out.println("\t\tEl Número es Positivo.");

    } else {
      
      if (numeroIf < 0) {
        System.out.println("\t\tEl Número es Negativo.");

      } else {
        System.out.println("\t\tEl Número es Cero.");
      }
    }

    
    /*
      Crea un bucle While, este bucle tendrá que tener como condición que la 
      variable numeroWhile sea inferior a 3, el bloque de código que tendrá el 
      bucle deberá: 

      - Incrementar el valor de la variable en uno cada vez que se ejecute.
      - Mostrarlo por pantalla cada vez que se ejecute.
    */
    System.out.println("\n\t2) While:\n" );

    int numeroWhile = 0;

    while (numeroWhile < 3) {
      
      System.out.println("\t\tEl valor de numeroWhile es: " + numeroWhile);

      numeroWhile++;
    }

    
    /*
      Para el bucle Do While, deberás crear la misma estructura que en el 
      While, pero solo se debe ejecutar una vez.
    */
    System.out.println("\n\t3) Do While:\n" );

    numeroWhile = 2;

    do {
      System.out.println("\t\tEl valor de numeroWhile es: " + numeroWhile);

      numeroWhile++;

    } while (numeroWhile < 3);

    
    /*
      Para el bucle For, crea una variable numeroFor, esta variable tendrá como
      valor 0 y su condición será que la variable sea igual o menor que 3, se 
      irá incrementando en 1 su valor cada vez que se ejecute y deberá 
      mostrarse por pantalla.
    */
    System.out.println("\n\t4) For:\n" );

    for (int numeroFor = 0; numeroFor <= 3; numeroFor++ ) {
      System.out.println("\t\tEl valor de numeroFor es: " + numeroFor);
    }


    /*
      Por último, para el Switch, deberás crear la variable estacion, y 
      distintos case para las cuatro estaciones del año. Dependiendo del valor
      de la variable estacion se deberá mandar un mensaje por consola 
      informando de la estación en la que está. También habrá que poner un 
      default para cuando el valor de la variable no sea una estación.
    */
    System.out.println("\n\t5) Switch:\n" );

    String estacion = "INVIERNO";

    switch (estacion) {
      case "VERANO":
        System.out.println("\t\tLa Estación es: " + estacion);
        break;

      case "OTOÑO":
        System.out.println("\t\tLa Estación es: " + estacion);
        break;

      case "INVIERNO":
        System.out.println("\t\tLa Estación es: " + estacion);
        break;

      case "PRIMAVERA":
        System.out.println("\t\tLa Estación es: " + estacion);
        break;

      default:
        System.out.println(estacion + 
          "\t\tNo es un Valor Válido para una Estación");
        break;

    }
  }
}