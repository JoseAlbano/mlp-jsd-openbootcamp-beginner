package topic02_ide_files_syntax.com.josealbano.javabasico.tema1;

/* 
  Curso: Java Básico.
  Tema 2: IDE, Archivos Java y Sintaxis

  Ejercicio Tema 2:

  Para este primer ejercicio tendréis que realizar lo siguiente:

  - Crea un proyecto de Java desde 0.
  - Dentro del proyecto tenéis que crear un paquete. En el paquete tendréis que
    crear una clase.
  - Dentro de la clase tenéis que crear el método main e imprimir todos los 
    datos que se han visto en las sesiones.
    
    Recordatorio: Los tipos de datos más comunes son int, long, double, 
      boolean, String.
*/


/**
 *
 * @author José H. Albano
 */
public class TiposDeDatosApp {
  public static void main(String[] args) {
    System.out.println("*** Curso: Java Básico ***\n\n" +
      "--- Tema 2: IDE, Archivos Java y Sintaxis ---\n\n" +
      "Ejercicio Tema 1\n" );

    byte variableByte = 127;
    short variableShort = 30000;
    int variableInt = 1500123456;
    long variableLong = 100012345;
    
    float variableFloat = 8.78521f;
    double variableDouble = 3.147524621;
    
    boolean variableBoolean = true;
    
    char variableChar = 'J';
    
    String variableString = "José Albano";
    
    System.out.println("\tVariable byte: " + variableByte);
    System.out.println("\tVariable short: " + variableShort);
    System.out.println("\tVariable int: " + variableInt);
    System.out.println("\tVariable long: " + variableLong);
    System.out.println("\tVariable float: " + variableFloat);
    System.out.println("\tVariable double: " + variableDouble);
    System.out.println("\tVariable char: " + variableChar);
    System.out.println("\tVariable String: " + variableString);
    
  }
}