package topic03_functions;

/* 
  Curso: Java Básico.
  Tema 3: Funciones

  Ejercicio Tema 3:

  Para este ejercicio tendréis que crear una función que reciba un precio y 
  devuelva el precio con el IVA incluido.
*/
import java.util.Scanner;

public class PrecioApp {
  public static void main(String[] args) {
    System.out.println("*** Curso: Java Básico ***\n\n" +
      "--- Tema 3: Funciones ---\n\n" +
      "Ejercicio Tema 3\n" );

    Scanner input = new Scanner(System.in);
    double precioTotal;

    System.out.printf("\tIngrese un Precio: ");
    precioTotal = input.nextDouble();

    System.out.println("\n\tPrecio:\t" + precioTotal);
    System.out.println("\tPrecio Total:\t" + calcularPrecio(precioTotal));
  }

  public static double calcularPrecio(double precio) {
    double IVA = .21; // Valor del IVA en Argentina: 21%

    return precio + precio * IVA;
  }
}