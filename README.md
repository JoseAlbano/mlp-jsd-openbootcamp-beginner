# My Learning Path: Java Software Developer

## Bootcamp: *Open Bootcamp "Beginner Level" Roadmap* by *[open-bootcamp.com](https://open-bootcamp.com/)*

Hello.

This Bootcamp roadmap contains 9 Courses. Here are my Solutions to Courses Exercises.

### My Progress
#### Course names are in English and Spanish (The original language of the course)

- [x] Course 01: Programming Concepts (Conceptos de la Programación).
- [x] Course 02: Introduction to Programming (Introducción a la Programación)
- [ ] Course 03: Java Basics (Java Básico)
- [ ] Course 04: Java Avanzado (Advanced Java)
- [ ] Course 05: Spring
- [ ] Course 06: Testing with JUnit (Pruebas con JUnit)
- [ ] Course 07: HTML / CSS
- [ ] Course 08: JavaScript Basics (JavaScript Básico)
- [ ] Course 09: ReactJS
